package com.github.rizwanbinzahir.testngsample;

import java.util.stream.LongStream;

public class FactorialCalculator1 {

	public long getFactorial(long number) {
		if(number < 0) {
			throw new ArithmeticException("Invalid number");
		}
		
		if(number == 0 || number == 1) {
			return 1;
		}
		
		return LongStream.rangeClosed(1, number).reduce(1, (x, y) -> x * y);
	}
}
