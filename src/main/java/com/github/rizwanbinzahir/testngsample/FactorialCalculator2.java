package com.github.rizwanbinzahir.testngsample;

public class FactorialCalculator2 {
	
	public long getFactorial(long number) {
		if(number < 0) {
			throw new ArithmeticException("Invalid number");
		}
		
		if(number == 0 || number == 1) {
			return 1;
		}
		
		return number * getFactorial(number - 1);
	}
	
}
