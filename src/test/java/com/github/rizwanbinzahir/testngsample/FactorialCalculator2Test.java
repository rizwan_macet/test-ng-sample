package com.github.rizwanbinzahir.testngsample;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FactorialCalculator2Test {
	private FactorialCalculator2 calculator;
	
	@BeforeClass
	public void setUp() throws Exception {
		calculator = new FactorialCalculator2();
	}
	
	@Test
	public void testForNumber0() {
		var result = calculator.getFactorial(1);		
		assertEquals(result, 1);
	}
	
	@Test
	public void testForAnyNumber() {
		var result = calculator.getFactorial(5);		
		assertEquals(result, 120L);
	}
	
	@Test(expectedExceptions = ArithmeticException.class)
	public void testForInvalidNumber() {
		var result = calculator.getFactorial(-1);		
		assertEquals(result, 3628800L);
	}
}
