package com.github.rizwanbinzahir.testngsample;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;

public class TestNGEmailReport implements IReporter {

	private static final String ROW_TEMPLATE = "<tr class=\"%s\"><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>";

	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
		String reportTemplate = initReportTemplate();

		final String body = suites.stream().flatMap(suiteToResults()).collect(Collectors.joining());

		final String html = reportTemplate.replaceFirst("</tbody>", String.format("%s</tbody>", body));

		System.out.println("**************************************");
		System.out.println(html);
		System.out.println("**************************************");

		int totalPassed  = 0;
		int totalFailed  = 0;
		int totalSkipped = 0;
		
		
		
		List<TestResultDto> results = suites.stream()
		.flatMap(suite -> suite.getResults().values().stream())
		.map(suiteResult -> {
			int failed = suiteResult.getTestContext().getFailedTests().getAllMethods().size();
			int passed = suiteResult.getTestContext().getPassedTests().getAllMethods().size();
			int skipped = suiteResult.getTestContext().getSkippedTests().getAllMethods().size();
			
			return new TestResultDto(failed, passed, skipped);
		}).collect(Collectors.toList());
		
		
		for (TestResultDto testResultDto : results) {
			totalPassed += testResultDto.getPassed();
			totalFailed += testResultDto.getFailed();
			totalSkipped += testResultDto.getSkipped();
		}
		
		System.out.println("failed " + totalFailed);
		System.out.println("passed " + totalPassed);
		System.out.println("skipped " + totalSkipped);

	}

	private static class TestResultDto {
		private int failed;
		private int passed;
		private int skipped;
		
		public TestResultDto(int failed, int passed, int skipped) {
			super();
			this.failed = failed;
			this.passed = passed;
			this.skipped = skipped;
		}

		public int getFailed() {
			return failed;
		}

		public int getPassed() {
			return passed;
		}

		public int getSkipped() {
			return skipped;
		}

	}

	private Function<ISuite, Stream<? extends String>> suiteToResults() {
		return suite -> suite.getResults().entrySet().stream().flatMap(resultsToRows(suite));
	}

	private Function<Map.Entry<String, ISuiteResult>, Stream<? extends String>> resultsToRows(ISuite suite) {
		return e -> {
			ITestContext testContext = e.getValue().getTestContext();

			Set<ITestResult> failedTests = testContext.getFailedTests().getAllResults();
			Set<ITestResult> passedTests = testContext.getPassedTests().getAllResults();
			Set<ITestResult> skippedTests = testContext.getSkippedTests().getAllResults();

			String suiteName = suite.getName();

			return Stream.of(failedTests, passedTests, skippedTests)
					.flatMap(results -> generateReportRows(e.getKey(), suiteName, results).stream());

		};
	}

	private Function<Map.Entry<String, ISuiteResult>, Stream<? extends String>> resultsToDto(ISuite suite) {
		return e -> {
			ITestContext testContext = e.getValue().getTestContext();

			Set<ITestResult> failedTests = testContext.getFailedTests().getAllResults();
			Set<ITestResult> passedTests = testContext.getPassedTests().getAllResults();
			Set<ITestResult> skippedTests = testContext.getSkippedTests().getAllResults();

			String suiteName = suite.getName();

			return Stream.of(failedTests, passedTests, skippedTests)
					.flatMap(results -> generateReportRows(e.getKey(), suiteName, results).stream());
		};
	}

	private List<String> generateReportRows(String testName, String suiteName, Set<ITestResult> allTestResults) {
		return allTestResults.stream().map(testResultToResultRow(testName, suiteName)).collect(toList());
	}

	private Function<ITestResult, String> testResultToResultRow(String testName, String suiteName) {
		return testResult -> {
			switch (testResult.getStatus()) {
			case ITestResult.FAILURE:
				return String.format(ROW_TEMPLATE, "danger", suiteName, testName, testResult.getName(), "FAILED", "NA");

			case ITestResult.SUCCESS:
				return String.format(ROW_TEMPLATE, "success", suiteName, testName, testResult.getName(), "PASSED",
						String.valueOf(testResult.getEndMillis() - testResult.getStartMillis()));

			case ITestResult.SKIP:
				return String.format(ROW_TEMPLATE, "warning", suiteName, testName, testResult.getName(), "SKIPPED",
						"NA");

			default:
				return "";
			}
		};
	}

	private String initReportTemplate() {
		String template = null;
		byte[] reportTemplate;
		try {
			reportTemplate = Files.readAllBytes(Paths.get("src/test/resources/reportTemplate.html"));
			template = new String(reportTemplate, "UTF-8");
		} catch (IOException e) {
			// LOGGER.error("Problem initializing template", e);
			e.printStackTrace();
		}
		return template;
	}
}
