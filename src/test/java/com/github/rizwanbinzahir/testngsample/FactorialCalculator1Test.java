package com.github.rizwanbinzahir.testngsample;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FactorialCalculator1Test {
	
	private FactorialCalculator1 calculator;
	
	@BeforeClass
	public void setUp() throws Exception {
		calculator = new FactorialCalculator1();
	}
	
	@Test
	public void testForNumber0() {
		var result = calculator.getFactorial(1);		
		assertEquals(result, 1);
	}
	
	@Test
	public void testForAnyNumber() {
		var result = calculator.getFactorial(10);		
		assertEquals(result, 3628800L);
	}
	
	@Test(expectedExceptions = ArithmeticException.class)
	public void testForInvalidNumber() {
		var result = calculator.getFactorial(-1);		
		assertEquals(result, 3628800L);
	}
	
}
